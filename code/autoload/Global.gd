extends Node

const fullCasetteArray := []
var clipList: Array

const startCash := 220
const priceMax := 120
const priceMin := 90

const winMax := 90
const winMin := 60

var rng := RandomNumberGenerator.new()


var playerInventory: Array
var store: Array
var storePrices: Array
var cash: float


func recalculateStore():
	store = []
	#var tmp := Clip.new({}, "res://assets/Doge/Doge.tscn")
	#tmp.price = 69
	for i in range(0,5):
		store.append(randomNewClip())  #tmp)


func randomCasette() -> Casette:
	return Casette.new("New",Color(0,0,0),[])

func randomClip() -> Clip:
	var c := clipList.size()-1
	print("clipcount %s" % c)
	return clipList[rng.randi_range(0,c)]

func randomNewClip() -> Clip:
	print("inv count %s" % playerInventory.size())
	print("cl  count %s" % clipList.size())
	if playerInventory.size() == clipList.size():
		return randomClip()
	var tmp := randomClip()
	while playerInventory.find(tmp) != -1:
		tmp = randomClip()
	return tmp

func populateClipList():
	cash = startCash
	rng.seed = OS.get_unix_time()
	var tmp: Clip

	tmp = Clip.new({}, "res://assets/Doge/Doge.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Boda/Boda.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Explosion/Explosion.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Heart/Heart.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Novios/Novios.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Patricio/Patricio.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Penguin/Penguin.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	print("\n a %s" % tmp.price)
	clipList.append(tmp)

	tmp = Clip.new({}, "res://assets/Perrito/Perrito.tscn")
	tmp.price = rng.randi_range(priceMin, priceMax)
	clipList.append(tmp)
