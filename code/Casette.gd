extends Node

class_name Casette


var tapeName: String
var color: Color
var tape: Array # Array of Clips
var expectedRange:= {"min":{}, "max":{}} # Holds min and max values

func getTotalEmotion() -> Dictionary:
	return {}
	
func _init(tN:String, c:Color, t:Array):
	tapeName = tN
	color = c
	tape = t

