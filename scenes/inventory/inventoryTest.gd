extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#var tmp: Button
var casetteTemp: CasetteButton
var add: bool
var count: int
var vBox: VBoxContainer
var closeButton: Button
var callerButton: Node
var callerLabel: Node
var clipPreview: Sprite
var selectedClipsPlaceholders: Array
var inventoryClips: Array


func init(caller: Node, label: Node, preview: Sprite, selectedPlaceholders: Array):
	self.callerButton = caller
	self.callerLabel = label
	self.clipPreview = preview
	self.selectedClipsPlaceholders = selectedPlaceholders

# Called when the node enters the scene tree for the first time.
func _ready():
	vBox = get_node("ScrollContainer/VBoxContainer")
	closeButton = get_node("TextureButton")
	closeButton.connect("button_up", self, "closeButtonAction")
	count = 0
	inventoryClips = Global.playerInventory
	for clip in inventoryClips:
		casetteTemp = CasetteButton.new(clip)
		casetteTemp.connect("mouse_released", self, "setClipInPlaceholder", [clip.sprite, count])
		

func setClipInPlaceholder(sprite: String, index: int):
	var spritSource = load(sprite)
	#selectedClipsPlaceholders[index] =  AnimatedSprite.new()
	selectedClipsPlaceholders[index].frames = spritSource.instance().frames
	selectedClipsPlaceholders[index].show()
	selectedClipsPlaceholders[index].play()

func input_check():
   # Mouse in viewport coordinates
	#TODO
	
	
	if Input.is_action_just_released("ui_accept"):
		
		
		#casetteTemp = CasetteButton.new(clipTestModel)
		var tmpclip = Global.randomClip()
		casetteTemp = CasetteButton.new(tmpclip)
		
		add = true

		casetteTemp.connect("button_up",self,"setClipInPlaceholder",[tmpclip.sprite, count])
		casetteTemp.connect("mouse_entered", self, "displayPreview")
		casetteTemp.connect("mouse_exited", self, "hidePreview")
		count = count+1
		
		print("boop")
	else:
		pass
		
func displayPreview():
	self.clipPreview.texture = load("res://assets/Doge/1.png")
	self.clipPreview.show()
	

func hidePreview():
	self.clipPreview.hide()


func closeButtonAction():
	hide()
	callerButton.show()
	callerLabel.show()
	

func buttonPrint(button: CasetteButton):
	print(button.clipModel)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	input_check()
	if add:
		add = false
		vBox.add_child(casetteTemp)
		#vBox.add_child(tmp)
		print("added child")
		print(vBox.to_string())

