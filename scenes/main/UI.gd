extends Control

var inventoryButton: TextureButton
var inventoryLabel: Label
var junkyardButton: TextureButton
var clipPreview: Sprite
var selectedClipsPlaceholders: Array

const InventoryScene := preload("res://scenes/inventory/inventory.tscn")
const JunkyardScene := preload("res://scenes/junkyard/junkyard.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	inventoryButton = get_node("InventoryButtonContainer/InventoryButton")
	inventoryButton.connect("button_up", self, "_on_InventoryButton_pressed",[inventoryButton])
	inventoryLabel = get_node("InventoryButtonContainer/InventoryLabel")
	
	junkyardButton = get_node("JunkjardButtonContainer/JunkjardButton")
	junkyardButton.connect("button_up", self, "_on_JunkyardButton_release")
	
	clipPreview = get_node("previewSprite")
	
	selectedClipsPlaceholders = [get_node("tapeRepairing/Container/AnimatedSprite0"), 
								get_node("tapeRepairing/AnimatedSprite1"),
								get_node("tapeRepairing/AnimatedSprite2"),
								get_node("tapeRepairing/AnimatedSprite3"),
								get_node("tapeRepairing/AnimatedSprite4"),
								get_node("tapeRepairing/AnimatedSprite5"),
								get_node("tapeRepairing/AnimatedSprite6")]


#Shows the inventory when the inventory button is pressed
func _on_InventoryButton_pressed(button:TextureButton):
	print("Inventory button pressed")
	var inventory = InventoryScene.instance()
	inventory.init(inventoryButton, inventoryLabel, clipPreview, selectedClipsPlaceholders)
	add_child(inventory)
	inventory.set_position(button.rect_position)
	inventory.show()

	inventoryButton.hide()
	inventoryLabel.hide()

func _on_JunkyardButton_release():
	print("boop")
	var junkyard = JunkyardScene.instance()
	get_parent().add_child(junkyard)
	junkyard.show()
	pass

