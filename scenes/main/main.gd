extends Node


var transitionSong:AudioStreamPlayer
var mainSongLoop: AudioStreamPlayer

onready var fadeInAudio := get_node("audioTween")
export var transition_duration := 1.00
export var transition_type := 1 # TRANS_SINE

func _ready():
	transitionSong = get_node("transition")
	#transitionSong.stream = load("res://assets/Luciid_transition.ogg")
	transitionSong.connect("finished", self, "songLoop")
	#transitionSong.volume_db = -16
	#transitionSong.pitch_scale = 1.2
	mainSongLoop = get_node("loop")
	#mainSongLoop.stream = load("res://assets/Luciid_loop.ogg")
	mainSongLoop.connect("finished", self, "songLoop")
	mainSongLoop.volume_db = -16
	mainSongLoop.pitch_scale = 1
	transitionSong.play(0)
	fadeInSong(transitionSong)

func songLoop():
	print("yay")
	transitionSong.stop()
	mainSongLoop.play(0)


func fadeInSong(stream:AudioStreamPlayer):
	fadeInAudio.interpolate_property(stream, "volume_db", -30, -16, transition_duration, transition_type, Tween.EASE_OUT, 0)
	fadeInAudio.start()
