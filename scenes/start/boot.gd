extends Control

#var splashScreen
#var startButton: TextureButton
onready var startButton := get_node("StartScreen/VBoxContainer/StartButton")
#var quitButton: TextureButton
onready var quitButton := get_node("StartScreen/VBoxContainer/QuitButton")
onready var song := get_node("loop")


onready var fadeOutAudio := get_node("audioTween")
export var transition_duration := 1.00
export var transition_type := 1 # TRANS_SINE



func _ready():
	#startButton = get_node("StartScreen/VBoxContainer/StartButton")
	startButton.connect("button_up", self, "start")
	#quitButton = get_node("StartScreen/VBoxContainer/QuitButton")
	quitButton.connect("button_up", self, "quit")
	#song = get_node("loop")
	song.play(0)


func start():
	
	Global.populateClipList()
	Global.recalculateStore()
	print("changing scene")
	fadeOutSong(song)
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/main/main.tscn")
	
	
func quit():
	get_tree().quit()


func fadeOutSong(stream:AudioStreamPlayer):
	var currentVolume = stream.volume_db
	fadeOutAudio.interpolate_property(stream, "volume_db", currentVolume, -80, transition_duration, transition_type, Tween.EASE_OUT, 0)
	fadeOutAudio.start()
	fadeOutAudio.connect("tween_completed", self, "stopSong")

func stopSong():
	song.stop()
