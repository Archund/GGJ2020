extends Control


onready var itemContainer := get_node("Panel/GridContainer")
onready var backButton := get_node("TextureButton")
onready var cashLabel = get_node("Cash")
const storeItem = preload("res://scenes/junkyard/storeItem.tscn")

func _ready():
	print(Global.store)
	backButton.connect("button_up", self, "back")
	cashLabel.text = "$%s" % Global.cash
	for item in Global.store:
		var tmp := storeItem.instance()
		var button := tmp.get_node("TextureRect")
		button.connect("button_up", self, "buyCasette", [tmp])
		
		itemContainer.add_child(tmp)
		print("jk %s" % item.price)
		tmp.init(item)
		tmp.show()

func buyCasette(item):
	Global.cash -= item.item.price
	cashLabel.text = "$%s" % Global.cash
	
	
	
	

func back():
	self.hide()
	self.queue_free()
